#include <ntddk.h>
#include <wdf.h>

CHAR g_filterSignature[] = "+HandsOnFilter";

void HandsOnFilterReadCompletion(
    _In_  WDFREQUEST Request,
    _In_  WDFIOTARGET Target,
    _In_  PWDF_REQUEST_COMPLETION_PARAMS Params,
    _In_  WDFCONTEXT Context
    )
{
    NTSTATUS status;
    WDFMEMORY outputMemory;
    PCHAR buf;
    size_t bufSize;

    UNREFERENCED_PARAMETER(Target);
    UNREFERENCED_PARAMETER(Context);

    DbgPrint("HandsOnFilterReadCompletion enters\n");

    if (!NT_SUCCESS(Params->IoStatus.Status)) {
        goto Done;
    }

    status = WdfRequestRetrieveOutputMemory(Request, &outputMemory);

    if (!NT_SUCCESS(status)) {
        goto Done;
    }

    buf = WdfMemoryGetBuffer(outputMemory, &bufSize);
    if (buf != NULL &&
        bufSize > 0 &&
        strnlen(buf, bufSize) + sizeof(g_filterSignature) <= bufSize) {

        // For writes, change the data in-place before sending it down.
        // It's hacky but ok for buffered I/O.
        strcat(buf, g_filterSignature);
        
        WdfRequestSetInformation(Request, strlen(buf) + 1);
    }

Done: 
    WdfRequestComplete(Request, Params->IoStatus.Status);

    DbgPrint("HandsOnFilterReadCompletion exits\n");
}


VOID
HandsOnFilterEvtIoDefault(
    _In_  WDFQUEUE Queue,
    _In_  WDFREQUEST Request
)
{
    NTSTATUS status;
    WDFIOTARGET ioTarget;
    WDF_REQUEST_SEND_OPTIONS options;
    WDF_REQUEST_PARAMETERS params;
    WDFMEMORY inputMemory;
    PCHAR buf;
    size_t bufSize;

    DbgPrint("HandsOnFilterEvtIoDefault enters\n");

    WDF_REQUEST_PARAMETERS_INIT(&params);
    WdfRequestGetParameters(Request, &params);

    ioTarget = WdfDeviceGetIoTarget(WdfIoQueueGetDevice(Queue));

    switch (params.Type) {
    case WdfRequestTypeRead:
        // For reads, set the completion routine to make the change in the returning data.
        WdfRequestFormatRequestUsingCurrentType(Request);

        WdfRequestSetCompletionRoutine(Request, HandsOnFilterReadCompletion, NULL);
            
        if (FALSE == WdfRequestSend(Request, ioTarget, WDF_NO_SEND_OPTIONS)) {
            // the framework failed sending down the request.
            WdfRequestComplete(Request, WdfRequestGetStatus(Request));
        }

        break;

    case WdfRequestTypeWrite:
        
        status = WdfRequestRetrieveInputMemory(Request, &inputMemory);
        if (!NT_SUCCESS(status)) {
            WdfRequestComplete(Request, status);
            break;
        }
        
        buf = WdfMemoryGetBuffer(inputMemory, &bufSize);
        if (buf != NULL && 
            bufSize > 0 && 
            strnlen(buf,bufSize) + sizeof(g_filterSignature) <= bufSize) {
            
            // For writes, change the data in-place before sending it down.
            // It's hacky but ok for buffered I/O.
            strcat(buf, g_filterSignature);
        }

        // Fall through...

    default:
        // send and forget, no need to format
        WDF_REQUEST_SEND_OPTIONS_INIT(&options, WDF_REQUEST_SEND_OPTION_SEND_AND_FORGET);

        if (FALSE == WdfRequestSend(Request, ioTarget, &options)) {
            // the framework failed sending down the request.
            WdfRequestComplete(Request, WdfRequestGetStatus(Request));
        }
        break;
    }

    DbgPrint("HandsOnFilterEvtIoDefault exits\n");
}


NTSTATUS
HandsOnFilterEvtDeviceAdd(
    _In_     WDFDRIVER Driver,
    _Inout_  PWDFDEVICE_INIT DeviceInit
    )
{
    NTSTATUS status;
    WDFDEVICE device;
    WDF_IO_QUEUE_CONFIG queueConfig;

    UNREFERENCED_PARAMETER(Driver);

    DbgPrint("HandsOnFilterEvtDeviceAdd enters\n");

    WdfFdoInitSetFilter(DeviceInit);

    status = WdfDeviceCreate(&DeviceInit, WDF_NO_OBJECT_ATTRIBUTES, &device);

    if (!NT_SUCCESS(status)) {
        goto Done;
    }
    
    // Create the default queue 
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&queueConfig, WdfIoQueueDispatchParallel);

    queueConfig.EvtIoDefault = HandsOnFilterEvtIoDefault;

    status = WdfIoQueueCreate(
        device,
        &queueConfig,
        WDF_NO_OBJECT_ATTRIBUTES,
        NULL
        );

    if (!NT_SUCCESS(status)) {
        goto Done;
    }

Done:
    DbgPrint("HandsOnFilterEvtDeviceAdd exits\n");
    return status;
}


NTSTATUS
DriverEntry(
    _In_  PDRIVER_OBJECT DriverObject,
    _In_  PUNICODE_STRING RegistryPath
    )
{
    WDF_DRIVER_CONFIG config;
    WDF_OBJECT_ATTRIBUTES attributes;
    NTSTATUS status;

    DbgPrint("HandsOnFilter!DriverEntry enters\n");

    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);

    WDF_DRIVER_CONFIG_INIT(&config, HandsOnFilterEvtDeviceAdd);

    status = WdfDriverCreate(DriverObject,
        RegistryPath,
        WDF_NO_OBJECT_ATTRIBUTES,
        &config,
        WDF_NO_HANDLE);

    DbgPrint("HandsOnFilter!DriverEntry exits\n");

    return status;
}
