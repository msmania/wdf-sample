#include <ntddk.h>
#include <wdf.h>

void MyFilterReadCompletion(WDFREQUEST Request,
                            WDFIOTARGET Target,
                            PWDF_REQUEST_COMPLETION_PARAMS Params,
                            WDFCONTEXT Context) {
    UNREFERENCED_PARAMETER(Target);
    UNREFERENCED_PARAMETER(Context);

    NTSTATUS Status = STATUS_SUCCESS;
    size_t BufferSize = 0;
    PVOID Buffer = NULL;
    WDFMEMORY OutputMemory;

    if (!NT_SUCCESS(Params->IoStatus.Status)) {
        goto cleanup;
    }

    Status = WdfRequestRetrieveOutputMemory(Request, &OutputMemory);
    if (!NT_SUCCESS(Status)) {
        goto cleanup;
    }

    Buffer = WdfMemoryGetBuffer(OutputMemory, &BufferSize);
    if (BufferSize > 0) {
        ((PCHAR)Buffer)[0] -= 1;
        WdfRequestSetInformation(Request, BufferSize);
    }

cleanup:
    WdfRequestComplete(Request, Params->IoStatus.Status);
}

VOID MyFilterEvtIoDefault(
    _In_  WDFQUEUE Queue,
    _In_  WDFREQUEST Request
    ) {
    UNREFERENCED_PARAMETER(Queue);

    NTSTATUS Status = STATUS_SUCCESS;
    BOOLEAN Ret = FALSE;
    WDF_REQUEST_PARAMETERS Parameters;

    WDFIOTARGET IoTarget = WdfDeviceGetIoTarget(WdfIoQueueGetDevice(Queue));
    WDF_REQUEST_SEND_OPTIONS SendOptions;

    size_t BufferSize = 0;
    WDFMEMORY InputMemory;
    PVOID Buffer = NULL;

    WDF_REQUEST_PARAMETERS_INIT(&Parameters);
    WdfRequestGetParameters(Request, &Parameters);

    switch (Parameters.Type) {
    case WdfRequestTypeRead:
        WdfRequestFormatRequestUsingCurrentType(Request);
        WdfRequestSetCompletionRoutine(Request, MyFilterReadCompletion, WDF_NO_CONTEXT);

        Ret = WdfRequestSend(Request, IoTarget, WDF_NO_SEND_OPTIONS);

        if (Ret == FALSE) {
            Status = WdfRequestGetStatus(Request);
            DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestSend failed: Status = 0x%08x\n", Status);

            WdfRequestComplete(Request, Status);
        }
        break;
    case WdfRequestTypeWrite:
        Status = WdfRequestRetrieveInputMemory(Request, &InputMemory);
        if (!NT_SUCCESS(Status)) {
            WdfRequestComplete(Request, Status);
            break;
        }

        Buffer = WdfMemoryGetBuffer(InputMemory, &BufferSize);
        if (BufferSize > 0) {
            ((PCHAR)Buffer)[0] += 1;
            WdfRequestSetInformation(Request, BufferSize);
        }

        // break;
    default:
        WDF_REQUEST_SEND_OPTIONS_INIT(&SendOptions, WDF_REQUEST_SEND_OPTION_SEND_AND_FORGET);

        Ret = WdfRequestSend(Request, IoTarget, &SendOptions);
        if (!Ret) {
            Status = WdfRequestGetStatus(Request);
            WdfRequestComplete(Request, Status);
        }
        break;
    }
}

NTSTATUS MyFilterEvtDriverDeviceAdd(
    _In_     WDFDRIVER Driver,
    _Inout_  PWDFDEVICE_INIT DeviceInit
    ) {
    UNREFERENCED_PARAMETER(Driver);
    UNREFERENCED_PARAMETER(DeviceInit);

    NTSTATUS Status = STATUS_SUCCESS;
    WDFDEVICE Device;
    
    WDF_IO_QUEUE_CONFIG DefaultQueueConfig;

    //
    // I'm a filter driver
    //
    WdfFdoInitSetFilter(DeviceInit);

    //
    // create a device object
    //
    WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);

    Status = WdfDeviceCreate(&DeviceInit, WDF_NO_OBJECT_ATTRIBUTES, &Device);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceCreate failed - 0x%08x\n", Status);
        goto cleanup;
    }

    //
    // Create the default queue 
    //
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&DefaultQueueConfig, WdfIoQueueDispatchParallel);
    DefaultQueueConfig.EvtIoDefault = MyFilterEvtIoDefault;

    Status = WdfIoQueueCreate(Device, &DefaultQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, NULL);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate failed - 0x%08x\n", Status);
        goto cleanup;
    }

    DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "MyFilterEvtDriverDeviceAdd is done peacefully.\n", Status);

cleanup:
    return Status;
}

NTSTATUS
DriverEntry(
IN PDRIVER_OBJECT  DriverObject,
IN PUNICODE_STRING  RegistryPath
) {
    NTSTATUS Status = STATUS_SUCCESS;

    WDF_DRIVER_CONFIG DriverConfig;

    WDF_DRIVER_CONFIG_INIT(&DriverConfig, MyFilterEvtDriverDeviceAdd);

    Status = WdfDriverCreate(DriverObject, RegistryPath, WDF_NO_OBJECT_ATTRIBUTES, &DriverConfig, WDF_NO_HANDLE);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDriverCreate failed - 0x%08x\n", Status);
        goto cleanup;
    }

    DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "DriverEntry is done.\n", Status);

cleanup:
    return Status;
}

