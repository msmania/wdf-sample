#define INITGUID

#include <ntddk.h>
#include <wdf.h>

DEFINE_GUID(GUID_DEVINTERFACE_HANDSON,
    0x781EF630, 0x72B2, 0x11d2, 0xB8, 0x52, 0x00, 0xC0, 0x4F, 0xAD, 0x51, 0x71);
//{781EF630-72B2-11d2-B852-00C04FAD5171}

#define FILE_DEVICE_HANDSON         FILE_DEVICE_UNKNOWN

#define HANDSON_IOCTL(_index_) \
    CTL_CODE(FILE_DEVICE_HANDSON, _index_, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)

#define IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE     HANDSON_IOCTL (0x800)
#define IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE          HANDSON_IOCTL (0x801)
#define IOCTL_HANDSON_PENDING_IN_THE_DRIVER         HANDSON_IOCTL (0x802)

VOID MyEvtIoDeviceControl(
    _In_  WDFQUEUE Queue,
    _In_  WDFREQUEST Request,
    _In_  size_t OutputBufferLength,
    _In_  size_t InputBufferLength,
    _In_  ULONG IoControlCode
    ) {
    UNREFERENCED_PARAMETER(Queue);
    UNREFERENCED_PARAMETER(Request);
    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(InputBufferLength);
    UNREFERENCED_PARAMETER(IoControlCode);

    WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);
}

VOID MyEvtIoRead(
    _In_  WDFQUEUE Queue,
    _In_  WDFREQUEST Request,
    _In_  size_t Length
    ) {
    UNREFERENCED_PARAMETER(Queue);
    UNREFERENCED_PARAMETER(Length);

    NTSTATUS Status = STATUS_SUCCESS;
    WDFMEMORY OutputMemory;
    CHAR Somedata[] = "Hello, KMDF.";
    size_t BytesToRead = min(Length, sizeof(Somedata));

    Status = WdfRequestRetrieveOutputMemory(Request, &OutputMemory);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestRetrieveOutputMemory failed - 0x%08x\n", Status);
        goto cleanup;
    }

    Status = WdfMemoryCopyFromBuffer(OutputMemory, 0, Somedata, BytesToRead);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfMemoryCopyFromBuffer failed - 0x%08x\n", Status);
        goto cleanup;
    }

cleanup:
    WdfRequestCompleteWithInformation(Request, Status, BytesToRead);

    return;
}

VOID MyEvtIoWrite(
    _In_  WDFQUEUE Queue,
    _In_  WDFREQUEST Request,
    _In_  size_t Length
    ) {
    UNREFERENCED_PARAMETER(Queue);
    UNREFERENCED_PARAMETER(Length);

    NTSTATUS Status = STATUS_SUCCESS;
    size_t InputBufferSize = 0;
    PCHAR InputBufferToPrint = NULL;

    Status = WdfRequestRetrieveInputBuffer(Request, 0, &InputBufferToPrint, &InputBufferSize);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestRetrieveInputBuffer failed - 0x%08x\n", Status);
        goto cleanup;
    }

    if (InputBufferSize == 0) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "HnadsOnFdo> (null)\n", Status);
    }
    else
    {
        if (InputBufferToPrint[InputBufferSize - 1])
            InputBufferToPrint[InputBufferSize - 1] = 0;
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "HnadsOnFdo> %s\n", InputBufferToPrint);
    }

cleanup:
    WdfRequestCompleteWithInformation(Request, Status, InputBufferSize);
    return;
}

NTSTATUS MyEvtDriverDeviceAdd(
    _In_     WDFDRIVER Driver,
    _Inout_  PWDFDEVICE_INIT DeviceInit
    ) {
    UNREFERENCED_PARAMETER(Driver);

    NTSTATUS Status = STATUS_SUCCESS;
    WDFDEVICE Device;

    WDF_IO_QUEUE_CONFIG SequentialQueueConfig;
    WDFQUEUE SequentialQ;

    WDF_IO_QUEUE_CONFIG ParallelQueueConfig;
    WDFQUEUE ParallelQ;

    //
    // create a device object
    //
    WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);

    Status = WdfDeviceCreate(&DeviceInit, WDF_NO_OBJECT_ATTRIBUTES, &Device);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceCreate failed - 0x%08x\n", Status);
        goto cleanup;
    }

    //
    //  create a device interface associated
    //
    Status = WdfDeviceCreateDeviceInterface(Device, &GUID_DEVINTERFACE_HANDSON, NULL);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceCreateDeviceInterface failed - 0x%08x\n", Status);
        goto cleanup;
    }

    //
    // create a first sequential queue for WRITE and IOCTL request
    //
    WDF_IO_QUEUE_CONFIG_INIT(&SequentialQueueConfig, WdfIoQueueDispatchSequential);
    SequentialQueueConfig.EvtIoWrite = MyEvtIoWrite;
    SequentialQueueConfig.EvtIoDeviceControl = MyEvtIoDeviceControl;
    Status = WdfIoQueueCreate(Device, &SequentialQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, &SequentialQ);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate(1) failed - 0x%08x\n", Status);
        goto cleanup;
    }

    Status = WdfDeviceConfigureRequestDispatching(Device, SequentialQ, WdfRequestTypeWrite);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceConfigureRequestDispatching(W) failed - 0x%08x\n", Status);
        goto cleanup;
    }

    Status = WdfDeviceConfigureRequestDispatching(Device, SequentialQ, WdfRequestTypeDeviceControl);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceConfigureRequestDispatching(IOCTL) failed - 0x%08x\n", Status);
        goto cleanup;
    }

    //
    // create a second sequential queue for READ request
    //
    WDF_IO_QUEUE_CONFIG_INIT(&ParallelQueueConfig, WdfIoQueueDispatchParallel);
    ParallelQueueConfig.EvtIoRead = MyEvtIoRead;
    Status = WdfIoQueueCreate(Device, &ParallelQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, &ParallelQ);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate(2) failed - 0x%08x\n", Status);
        goto cleanup;
    }

    Status = WdfDeviceConfigureRequestDispatching(Device, ParallelQ, WdfRequestTypeRead);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDeviceConfigureRequestDispatching(R) failed - 0x%08x\n", Status);
        goto cleanup;
    }

    DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "MyEvtDriverDeviceAdd is done peacefully.\n", Status);

cleanup:
    return Status;
}

NTSTATUS
DriverEntry(
    IN PDRIVER_OBJECT  DriverObject,
    IN PUNICODE_STRING  RegistryPath
    ) {
    NTSTATUS Status = STATUS_SUCCESS;

    WDF_DRIVER_CONFIG DriverConfig;

    WDF_DRIVER_CONFIG_INIT(&DriverConfig, MyEvtDriverDeviceAdd);

    Status = WdfDriverCreate(DriverObject, RegistryPath, WDF_NO_OBJECT_ATTRIBUTES, &DriverConfig, WDF_NO_HANDLE);
    if (!NT_SUCCESS(Status)) {
        DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfDriverCreate failed - 0x%08x\n", Status);
        goto cleanup;
    }

    DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "DriverEntry is done.\n", Status);

cleanup:
    return Status;
}
