// HandsOnApp.cpp : Defines the entry point for the console application.
//
#define INITGUID

#include "stdafx.h"

DEFINE_GUID(GUID_DEVINTERFACE_HANDSON,
    0x781EF630, 0x72B2, 0x11d2, 0xB8, 0x52, 0x00, 0xC0, 0x4F, 0xAD, 0x51, 0x71);
//{781EF630-72B2-11d2-B852-00C04FAD5171}


#define FILE_DEVICE_HANDSON         FILE_DEVICE_UNKNOWN

#define HANDSON_IOCTL(_index_) \
    CTL_CODE(FILE_DEVICE_HANDSON, _index_, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)

#define IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE     HANDSON_IOCTL (0x800)
#define IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE          HANDSON_IOCTL (0x801)
#define IOCTL_HANDSON_PENDING_IN_THE_DRIVER         HANDSON_IOCTL (0x802)


INT __cdecl
main(
_In_ ULONG argc,
_In_reads_(argc) PCHAR argv[]
)
{
    HDEVINFO                            hardwareDeviceInfo;
    SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
    ULONG                               predictedLength = 0;
    ULONG                               requiredLength = 0, bytes = 0;
    HANDLE                              file;
    int                                 i,ch;
    char                                buffer[100];


    //
    // Open a handle to the device interface information set of all
    // present HandsOn device interface.
    //

    hardwareDeviceInfo = SetupDiGetClassDevs(
        (LPGUID)&GUID_DEVINTERFACE_HANDSON,
        NULL, // Define no enumerator (global)
        NULL, // Define no
        (DIGCF_PRESENT | // Only Devices present
        DIGCF_DEVICEINTERFACE)); // Function class devices.
    if (INVALID_HANDLE_VALUE == hardwareDeviceInfo)
    {
        printf("SetupDiGetClassDevs failed: %x\n", GetLastError());
        return 0;
    }

    deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

    printf("\nList of HandsOn Device Interfaces\n");
    printf("---------------------------------\n");

    i = 0;

    //
    // Enumerate devices of toaster class
    //

    for (;;) {
        if (SetupDiEnumDeviceInterfaces(hardwareDeviceInfo,
            0, // No care about specific PDOs
            (LPGUID)&GUID_DEVINTERFACE_HANDSON,
            i, //
            &deviceInterfaceData)) {

            if (deviceInterfaceDetailData) {
                free(deviceInterfaceDetailData);
                deviceInterfaceDetailData = NULL;
            }

            //
            // Allocate a function class device data structure to
            // receive the information about this particular device.
            //

            //
            // First find out required length of the buffer
            //

            if (!SetupDiGetDeviceInterfaceDetail(
                hardwareDeviceInfo,
                &deviceInterfaceData,
                NULL, // probing so no output buffer yet
                0, // probing so output buffer length of zero
                &requiredLength,
                NULL)) { // not interested in the specific dev-node
                if (ERROR_INSUFFICIENT_BUFFER != GetLastError()) {
                    printf("SetupDiGetDeviceInterfaceDetail failed %d\n", GetLastError());
                    SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
                    return FALSE;
                }

            }

            predictedLength = requiredLength;

            deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(predictedLength);

            if (deviceInterfaceDetailData) {
                deviceInterfaceDetailData->cbSize =
                    sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
            }
            else {
                printf("Couldn't allocate %d bytes for device interface details.\n", predictedLength);
                SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
                return FALSE;
            }


            if (!SetupDiGetDeviceInterfaceDetail(
                hardwareDeviceInfo,
                &deviceInterfaceData,
                deviceInterfaceDetailData,
                predictedLength,
                &requiredLength,
                NULL)) {
                printf("Error in SetupDiGetDeviceInterfaceDetail\n");
                SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
                free(deviceInterfaceDetailData);
                return FALSE;
            }
            printf("%d) %ws\n", ++i,
                deviceInterfaceDetailData->DevicePath);
        }
        else if (ERROR_NO_MORE_ITEMS != GetLastError()) {
            free(deviceInterfaceDetailData);
            deviceInterfaceDetailData = NULL;
            continue;
        }
        else
            break;

    }


    SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);

    if (!deviceInterfaceDetailData)
    {
        printf("No device interfaces present\n");
        return 0;
    }

    //
    // Open the last toaster device interface
    //

    printf("\nOpening the last interface:\n %ws\n",
        deviceInterfaceDetailData->DevicePath);

    file = CreateFile(deviceInterfaceDetailData->DevicePath,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL, // no SECURITY_ATTRIBUTES structure
        OPEN_EXISTING, // No special create flags
        0, // No special attributes
        NULL);

    if (INVALID_HANDLE_VALUE == file) {
        printf("Error in CreateFile: %x", GetLastError());
        free(deviceInterfaceDetailData);
        return 0;
    }


    //
    // Read/Write to the toaster device.
    //

    printf("\n"
           "Press 'r' to read.\n"
           "Press 'w' to write.\n"
           "Press '1' to send IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE.\n"
           "Press '2' to send IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE.\n"
           "Press 'p' to send IOCTL_HANDSON_PENDING_IN_THE_DRIVER. (program will hang.)\n"
           "Press 'q' to exit...\n");
    fflush(stdin);
    do {
        ch = tolower(_getche());

        switch (ch) {
        case 'r':
            if (!ReadFile(file, buffer, sizeof(buffer), &bytes, NULL))
            {
                printf("Error in ReadFile: 0x%x\n", GetLastError());
                break;
            }
            printf("Read Successful. Got %d bytes: [%s]\n", bytes, buffer);
            break;
        case 'w':
            strcpy_s(buffer, "HandsOnApp");
            // We provide the whole buffer, since in this sample, we'd like to leave the rest for the filter driver to fill.
            if (!WriteFile(file, buffer, sizeof(buffer), NULL, NULL))
            {
                printf("Error in WriteFile: 0x%x\n", GetLastError());
                break;
            }
            printf("Write Successful.\n");
            break;
        case '1':
            if (!DeviceIoControl(file,
                    IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE,
                    NULL, 0,
                    NULL, 0,
                    &bytes, NULL)) {
                printf("IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE failed:0x%x\n", GetLastError());
            }
            else {
                printf("IOCTL_HANDSON_DONT_DISPLAY_IN_UI_DEVICE completed successfully\n");
            }
            break;
        case '2':
            if (!DeviceIoControl(file,
                IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE,
                NULL, 0,
                NULL, 0,
                &bytes, NULL)) {
                printf("IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE failed:0x%x\n", GetLastError());
            }
            else {
                printf("IOCTL_HANDSON_DISPLAY_IN_UI_DEVICE completed successfully\n");
            }
            break;
        case 'p':
        {
            printf("Sending IOCTL_HANDSON_PENDING_IN_THE_DRIVER\n");
            if (!DeviceIoControl(file,
                IOCTL_HANDSON_PENDING_IN_THE_DRIVER,
                NULL, 0,
                NULL, 0,
                &bytes, NULL)) {
                printf("IOCTL_HANDSON_PENDING_IN_THE_DRIVER failed:0x%x\n", GetLastError());
            }
            else {
                printf("IOCTL_HANDSON_PENDING_IN_THE_DRIVER completed successfully\n");
            }
        }
            break;
        }
    } while (ch != 'q');

    free(deviceInterfaceDetailData);
    CloseHandle(file);
    return 0;
}






